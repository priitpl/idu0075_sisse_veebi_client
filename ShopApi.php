<?php
/**
 * Created by PhpStorm.
 * User: priitpl
 * Date: 21.01.14
 * Time: 11:41
 */

class ShopApi {

    const WSDL = 'http://192.168.56.1:8080/shopApp/shopService?wsdl';

    private $client;

    public function __construct(){
        ini_set("soap.wsdl_cache_enabled", 0);

        try { $this->client = new SoapClient(self::WSDL, array("trace" => 1, "exception" => 1, 'cache_wsdl' => WSDL_CACHE_NONE));
        } catch(Exception $e){
            throw $e;
        }
    }

    public function getProducts($filter = null) {

//        $auth         = new ChannelAdvisorAuth($devKey, $password);
        $header     = new SoapHeader("http://www.example.com/webservices/", "APICredentials");
        $products = array();
        $products = $this->client->FindProducts(array(
            "keyword" => "Note 3",
            'StockSymbol' => 'TEST_STR','LicenseKey' => 'TEST_STR'
        ));
        return $products;
    }

    public function getCategories($filter){
        $categories = array();

        return $categories;
    }
} 