
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link href="static/css/bootstrap.css" rel="stylesheet">
    <style>
        .method>h2{padding:0px;}
        .method>h2{padding-bottom: 15px; border-bottom: 1px solid darkgray;}
    </style>
    <link rel="alternate stylesheet" href="http://yandex.st/highlightjs/7.5/styles/default.min.css">
    <link rel="alternate stylesheet" title="Visual Studio" href="http://highlightjs.org/static/styles/vs.css">
    <link rel="stylesheet" title="Rainbow" href="http://highlightjs.org/static/styles/rainbow.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    <script type="text/javascript">
        function htmlEncode(value){
            //create a in-memory div, set it's inner text(which jQuery automatically encodes)
            //then grab the encoded contents back out.  The div never exists on the page.
            return $('<div/>').text(value).html();
        }

        function htmlDecode(value){
            return $('<div/>').html(value).text();
        }
        function loadFileToElement(filename, callback)
        {
            var xmlHTTP = new XMLHttpRequest();
            try
            {
                xmlHTTP.open("GET", filename, false);
                xmlHTTP.send(null);
            }
            catch (e) {
                window.alert("Unable to load the requested file.");
                return;
            }
            callback(xmlHTTP)
        }
    </script>
    <style>
        .panel .panel-heading h4{margin-right: 10px;}
        .panel .panel-heading h4{display: inline;}
    </style>
</head>
<body>
<div class="container">

    <nav class="navbar navbar-default" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Shopaholic</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Link</a></li>
                <li><a href="#">Link</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Profile <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>


    <div class="row">
        <div class="col-lg-3 col-sm-3 col-xs-12 sidebar-offcanvas">
            <form action="?a=get.products.list">
                <p class="hidden-sm">
                    <img src="images/Retail-shop-icon.png" class="img-thumbnail col-md-12"/>
                </p>
                <div class="form-group">
                    <label for="exampleInputPassword1">Keyowrd</label>
                    <input type="text" class="form-control" id="keywordField" name="keyword" placeholder="Keyword">
                </div>

                <div class="row">
                    <div class="col-sm-5 text-center"><label for="minPriceField">Min Price</label></div>
                    <div class="col-sm-2"></div>
                    <div class="col-sm-5 text-center"><label for="maxPriceField">Max Price</label></div>
                </div>
                <div class="row">
                    <div class="col-sm-5"><input type="text" id="minPriceField" name="min_price" class="form-control col-md-12" value=""/></div>
                    <div class="col-sm-2"><=></div>
                    <div class="col-sm-5"><input type="text" id="maxPriceField" name="max_price" class="form-control col-md-12"  value=""/></div>
                </div>

                <hr/>
                <p style="margin-top:20px;">
                <button type="button" class="btn btn-primary btn-lg btn-block" id="searchButton">Search</button>
                </p>
            </form>

        </div>
        <div class="col-lg-9 col-sm-9 col-xs-12">

            <ul style="padding:0px;" class="products" id="productsUL">
                <?php for($i = 0; $i < 10; $i++){?>
                <li class="product col-lg-3 col-md-4 col-sm-4 col-xs-2 thumbnail">
                    <img src="images/1000x1000.png" />
                </li>
                <?php }?>
            </ul>
        </div>

    </div>
</div>

<script src="static/js/bootstrap.js"></script>
<script src="static/js/collapse.js"></script>
<script src="http://yandex.st/highlightjs/7.5/highlight.min.js"></script>
<script>
productTemplate = function(params){
    var temp = '<li class="product col-lg-3 col-md-4 col-sm-4 col-xs-2 thumbnail">';
    temp += '<img src="' + params.imageUrl +  '" /></li>';
    return temp;
}
    $('#accordion').collapse({
        toggle: false
    })
    $('#searchButton').click(function(e){
        $.ajax({
            url: $('form').attr('action'),
            data: $('form').serializeArray()
//            beforeSend: function( xhr ) {
//                xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
//            }
        })
        .done(function(data) {
            if (data.success == true) {
                if (data.products.length == 0) return;
                $('#productsUL').text('');
                var productsObj = data.products.products.product;
                console.log(productsObj.length);
                console.log(productsObj);
                for (var i = 0; i < productsObj.length; i ++) {
                    $('#productsUL').append(productTemplate(productsObj[i]));
                }
            } else {
            }
        }).error(function(data){
            alert(data.error);
        });
    });
</script>
<script>hljs.initHighlightingOnLoad();</script>
</body>
</html>